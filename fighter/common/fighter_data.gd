class_name FighterData
extends Resource


@export var fighter_scene: PackedScene
@export var avatar_scene: PackedScene
@export var portrait_texture: Texture2D
