class_name State
extends Node


@onready var fighter := Entity.get_entity(self)


## handles transitions to other states
func handle_input(input: InputManager):
	_handle_input(input)


## handles updates in state
func update(delta, input: InputManager):
	_update(delta, input)


func enter_state():
	_enter_state()
	
	
func _handle_input(_input: InputManager):
	push_error("Virtual method 'handle_input'")
	return


func _update(_delta, _input: InputManager):
	push_error("Virtual method 'update'")
	return


func _enter_state():
	push_error("Virtual method 'enter_state'")
	return
