class_name Avatar
extends Node2D

signal hit(other)

enum FaceDir {
	LEFT,
	RIGHT
}

enum AnimationType {
	IDLE,
	WALK,
	GUARD,
	STAGGER,
	PRONE,
	DODGE,
	ATTACK_WEAK,
	ATTACK_MEDIUM,
	ATTACK_STRONG,
	WINDUP_WEAK,
	WINDUP_MEDIUM,
	WINDUP_STRONG,
	WIN,
}

const ANIMATION_NAMES := [
	&"idle",
	&"walk",
	&"guard",
	&"stagger",
	&"prone",
	&"dodge",
	&"attack_weak",
	&"attack_medium",
	&"attack_strong",
	&"windup_weak",
	&"windup_medium",
	&"windup_strong",
	&"win"
]

var player_side: Defs.Side:
	set(side):
		player_side = side
		match side:
			Defs.Side.LEFT:
				face(FaceDir.RIGHT)
				_hit_box.collision_layer = 1
				_hit_box.collision_mask = Defs.Layer.PLAYER_RIGHT
				_hurt_box.collision_layer = Defs.Layer.PLAYER_LEFT
				_hurt_box.collision_mask = 1
			Defs.Side.RIGHT:
				face(FaceDir.LEFT)
				_hit_box.collision_layer = 1
				_hit_box.collision_mask = Defs.Layer.PLAYER_LEFT
				_hurt_box.collision_layer = Defs.Layer.PLAYER_RIGHT
				_hurt_box.collision_mask = 1

@onready var _animation_player: AnimationPlayer = %AnimationPlayer
@export var _hit_box: Area2D
@export var _hurt_box: Area2D


func face(dir: FaceDir):
	match dir:
		FaceDir.LEFT:
			scale.x = -1
		FaceDir.RIGHT:
			scale.x = 1


func play(animation_type: AnimationType):
	_animation_player.play(ANIMATION_NAMES[animation_type])
