class_name Fighter
extends Entity

signal lives_changed(fighter, current_lives)
signal lost(fighter)


@export_category("BASE STATS")
@export var scaling: float = 0
@export var speed: float = 0
@export var dodging_speed: float = 0
@export var lives: int = 3
@export_category("COMPONENTS") 
@export var state_machine: StateMachine
@export var avatar: Avatar



func _on_state_machine_took_damage():
	lives -= 1
	lives_changed.emit(self, lives)
	print("tem ", lives, " vidas agora!")
	if lives <= 0:
		lost.emit(self)
