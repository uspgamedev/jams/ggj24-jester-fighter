class_name StandingState
extends State


func _handle_input(input: InputManager):
	# make so that the dictionary "input_buffer" is passed as argument to these functions
	var input_buffer := input.input_buffer
	
	if input_buffer["right"].holding:
		if %Avatar.player_side == Defs.Side.LEFT:
			get_parent().set_state("WalkForward")
		else:
			get_parent().set_state("WalkBackwards")
	if input_buffer["left"].holding:
		if %Avatar.player_side == Defs.Side.RIGHT:
			get_parent().set_state("WalkForward")
		else:
			get_parent().set_state("WalkBackwards")
	#if input_buffer[("light_attack"):
	if input_buffer["light_attack"].just_pressed:
		get_parent().set_state("LightAttack")
	if input_buffer["medium_attack"].just_pressed:
		get_parent().set_state("MediumAttack")
	if input_buffer["heavy_attack"].just_pressed:
		get_parent().set_state("HeavyAttack")
	if input_buffer["dodge"].just_pressed:
		get_parent().set_state("Dodging")
	
