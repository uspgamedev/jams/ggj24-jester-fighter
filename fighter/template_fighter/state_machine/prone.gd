extends State


var animation_cycles = 0

	 
func _enter_state():
	if not get_parent().avatar._animation_player.is_connected(&"animation_finished", _knockdown_time):
		get_parent().avatar._animation_player.connect(&"animation_finished", _knockdown_time)
	match get_parent().avatar.player_side:
		Defs.Side.LEFT:
			get_parent().avatar.face(get_parent().avatar.FaceDir.RIGHT)
		Defs.Side.RIGHT:
			get_parent().avatar.face(get_parent().avatar.FaceDir.LEFT)
	get_parent().avatar.play(get_parent().avatar.AnimationType.PRONE)
	

func _handle_input(_input: InputManager):
	pass


func _update(_delta, _input: InputManager):
	pass
	

func _exit_state():
	get_parent().set_state("Standing")


func _knockdown_time(prone_animation: String):
	if prone_animation == "prone":
		animation_cycles += 1
		print("cycle finished! ", animation_cycles)
		if animation_cycles == 1:
			animation_cycles = 0
			_exit_state()
		else:
			get_parent().avatar.play(get_parent().avatar.AnimationType.PRONE)
