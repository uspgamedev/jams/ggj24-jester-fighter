extends StandingState


func _enter_state():
	match get_parent().avatar.player_side:
		Defs.Side.LEFT:
			get_parent().avatar.face(Avatar.FaceDir.RIGHT)
		Defs.Side.RIGHT:
			get_parent().avatar.face(Avatar.FaceDir.LEFT)
	get_parent().avatar.play(Avatar.AnimationType.IDLE)


func _handle_input(input: InputManager):
	# make so that the dictionary "input_buffer" is passed as argument to these functions
	var input_buffer := input.input_buffer
	
	if input_buffer["right"].holding:
		if get_parent().avatar.player_side == Defs.Side.LEFT:
			get_parent().set_state("WalkForward")
		else:
			get_parent().set_state("WalkBackwards")
	if input_buffer["left"].holding:
		if get_parent().avatar.player_side == Defs.Side.RIGHT:
			get_parent().set_state("WalkForward")
		else:
			get_parent().set_state("WalkBackwards")
	#if input_buffer[("light_attack"):
	if input_buffer["light_attack"].just_pressed:
		get_parent().set_state("LightAttack")
	if input_buffer["medium_attack"].just_pressed:
		get_parent().set_state("MediumAttack")
	if input_buffer["heavy_attack"].just_pressed:
		get_parent().set_state("HeavyWindup")
	if input_buffer["dodge"].just_pressed:
		get_parent().set_state("Dodging")
	

func _update(_delta, _input: InputManager):
	pass
