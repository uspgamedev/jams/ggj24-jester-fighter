class_name StateMachine
extends Node


signal took_damage

@export var input: InputManager
@export var avatar: Avatar
var current_state: State


func _ready():
	avatar._hurt_box.connect(&"area_entered", _try_take_damage)
	set_state.bind("Standing").call_deferred()


func _process(delta):
	if current_state != null:
		current_state.handle_input(input)
		current_state.update(delta, input)
	#print(current_state)
	#print(avatar.player_side)


func set_state(state_name: String):
	var new_state = get_node(state_name)
	if new_state == null:
		push_error("state not in state machine!")
		return	
	new_state.enter_state()
	current_state = new_state
	#print(current_state)


func _try_take_damage(_area: Area2D):
	if current_state == $WalkBackwards:
		set_state("Blocking")
	else:
		print("took damage!!")
		set_state("Stagger")
		took_damage.emit()
