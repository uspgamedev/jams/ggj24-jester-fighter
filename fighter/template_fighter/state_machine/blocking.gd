extends State


func _enter_state():
	if not get_parent().avatar._animation_player.is_connected(&"animation_finished", _finish_block):
		get_parent().avatar._animation_player.connect(&"animation_finished", _finish_block)
	match get_parent().avatar.player_side:
		Defs.Side.LEFT:
			get_parent().avatar.face(get_parent().avatar.FaceDir.RIGHT)
		Defs.Side.RIGHT:
			get_parent().avatar.face(get_parent().avatar.FaceDir.LEFT)
	get_parent().avatar.play(get_parent().avatar.AnimationType.GUARD)
	
	
func _handle_input(_input: InputManager):
	pass
	
	
func _update(_delta, _input: InputManager):
	pass
	

func _exit_state():
	get_parent().set_state("Standing")


func _finish_block(attack_animation: String):
	if attack_animation == "guard":
		_exit_state()
