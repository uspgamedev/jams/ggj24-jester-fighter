extends State


var animation_cycles = 0


func _enter_state():
	if not get_parent().avatar._animation_player.is_connected(&"animation_finished", _windup_delay):
		get_parent().avatar._animation_player.connect(&"animation_finished", _windup_delay)
	match get_parent().avatar.player_side:
		Defs.Side.LEFT:
			get_parent().avatar.face(get_parent().avatar.FaceDir.RIGHT)
		Defs.Side.RIGHT:
			get_parent().avatar.face(get_parent().avatar.FaceDir.LEFT)
	get_parent().avatar.play(get_parent().avatar.AnimationType.WINDUP_STRONG)
	

func _handle_input(input: InputManager):
	var input_buffer = input.input_buffer
	
	if not input_buffer["heavy_attack"].holding:
		get_parent().set_state("HeavyAttack")


func _update(_delta, _input: InputManager):
	pass
	

func _exit_state():
	get_parent().set_state("HeavyAttack")


func _windup_delay(windup_animation: String):
	if windup_animation == "windup_strong":
		print("cycle finished! ", animation_cycles)
		animation_cycles += 1
		if animation_cycles == 3:
			animation_cycles = 0
			_exit_state()
		else:
			get_parent().avatar.play(get_parent().avatar.AnimationType.WINDUP_STRONG)
