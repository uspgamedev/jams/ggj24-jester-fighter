extends State


var animation_cycles = 0

	 
func _enter_state():
	if not get_parent().avatar._animation_player.is_connected(&"animation_finished", _lose_animation):
		get_parent().avatar._animation_player.connect(&"animation_finished", _lose_animation)
	match get_parent().avatar.player_side:
		Defs.Side.LEFT:
			get_parent().avatar.face(get_parent().avatar.FaceDir.RIGHT)
		Defs.Side.RIGHT:
			get_parent().avatar.face(get_parent().avatar.FaceDir.LEFT)
	get_parent().avatar.play(get_parent().avatar.AnimationType.PRONE)
	

func _handle_input(_input: InputManager):
	pass


func _update(_delta, _input: InputManager):
	pass
	

func _exit_state():
	pass


func _lose_animation(prone_animation: String):
	if prone_animation == "prone":
		get_parent().avatar.play(get_parent().avatar.AnimationType.PRONE)
