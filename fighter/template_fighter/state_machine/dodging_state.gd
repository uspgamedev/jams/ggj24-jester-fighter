extends State


var dodge_direction: int = 0


func _enter_state():
	if not get_parent().avatar._animation_player.is_connected(&"animation_finished", _finish_dodging):
		get_parent().avatar._animation_player.connect(&"animation_finished", _finish_dodging)
	# I don't like not using the InputManager, but let's leave like that 
	# for the time being
	if Input.is_action_pressed("left"):
		dodge_direction = -1
	elif Input.is_action_pressed("right"):
		dodge_direction = 1
	else:
		dodge_direction = 1 if get_parent().avatar.player_side == Defs.Side.LEFT else -1
	get_parent().avatar.play(get_parent().avatar.AnimationType.DODGE)


func _handle_input(_input: InputManager):
	pass
	
	
func _update(delta, _input: InputManager):
	fighter.position.x += dodge_direction * delta * fighter.dodging_speed
	
	
func _exit_state():
	get_parent().set_state("Standing")


func _finish_dodging(dodging_animation: String):
	if dodging_animation == "dodge":
		dodge_direction = 0
		_exit_state()
