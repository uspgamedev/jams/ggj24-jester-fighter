extends StandingState


func _enter_state():
	get_parent().avatar.play(get_parent().avatar.AnimationType.WALK)
	

func _handle_input(input: InputManager):
	var input_buffer := input.input_buffer
	
	if (
		(
			get_parent().avatar.player_side == Defs.Side.LEFT and	
	 		not input_buffer["right"].holding
		) or 
		(
			get_parent().avatar.player_side == Defs.Side.RIGHT and 
			not input_buffer["left"].holding
		)
	):
			get_parent().set_state("Standing")
	if input_buffer["light_attack"].just_pressed:
		get_parent().set_state("LightAttack")
	if input_buffer["medium_attack"].just_pressed:
		get_parent().set_state("MediumAttack")
	if input_buffer["heavy_attack"].just_pressed:
		get_parent().set_state("HeavyWindup")
	if input_buffer["dodge"].just_pressed:
		get_parent().set_state("Dodging")
	

func _update(delta, _input: InputManager):
	if get_parent().avatar.player_side == Defs.Side.LEFT:
		fighter.position.x += fighter.speed * delta
	elif get_parent().avatar.player_side == Defs.Side.RIGHT:
		fighter.position.x -= fighter.speed * delta
