extends State


var animation_cycles = 0

	 
func _enter_state():
	match get_parent().avatar.player_side:
		Defs.Side.LEFT:
			get_parent().avatar.face(get_parent().avatar.FaceDir.RIGHT)
		Defs.Side.RIGHT:
			get_parent().avatar.face(get_parent().avatar.FaceDir.LEFT)
	get_parent().avatar.play(get_parent().avatar.AnimationType.WIN)
	

func _handle_input(_input: InputManager):
	pass


func _update(_delta, _input: InputManager):
	pass
	

func _exit_state():
	pass
