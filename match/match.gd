class_name Match
extends Control


signal side_changed

@export var stage_scn: PackedScene
@export var fighter_left_data: FighterData
@export var fighter_right_data: FighterData
@export var player_input_scn: PackedScene
@export var cpu_input_scn: PackedScene
@export var ai_scn: PackedScene

var fighter_left: Fighter
var fighter_right: Fighter
var player_input: PlayerInput
var cpu_input: CPUInput
var ai: AI
var position_difference: float = -1
var time := 30.0

@onready var arena := %Arena
@onready var camera := %Camera
@onready var hud := %HUD

func _ready():
	assert(stage_scn, "Stage scene is missing!")
	assert(fighter_left_data, "Left fighter data is missing!")
	assert(fighter_right_data, "Right fighter data is missing!")

	BGM.play_battle_bgm()
	
	var stage := stage_scn.instantiate() as Stage
	fighter_left = fighter_left_data.fighter_scene.instantiate()
	fighter_right = fighter_right_data.fighter_scene.instantiate()
	player_input = player_input_scn.instantiate()
	cpu_input = cpu_input_scn.instantiate()
	ai = ai_scn.instantiate()
	ai.cpu_input = cpu_input
	
	fighter_left.add_child(player_input)
	fighter_right.add_child(cpu_input)
	fighter_right.add_child(ai)
	
	fighter_left.state_machine.input = player_input
	fighter_right.state_machine.input = cpu_input
	
	fighter_left.connect(&"lives_changed", _on_fighter_lives_changed)
	fighter_right.connect(&"lives_changed", _on_fighter_lives_changed)
	fighter_left.connect(&"lost", match_finished)
	fighter_right.connect(&"lost", match_finished)
	
	stage.place_fighter_left(fighter_left)
	stage.place_fighter_right(fighter_right)
	arena.add_child(stage)
	camera.position = get_viewport_rect().size / 2
	
	hud.setup_player_bars(fighter_left_data, fighter_right_data)
	hud.set_timer(ceili(time))
	hud.pre_match_text.connect(&"countdown_finished", _on_countdown_finished)
	
	set_process(false)
	fighter_left.state_machine.set_process(false)
	fighter_right.state_machine.set_process(false)


func _process(_delta):
	time -= _delta
	hud.set_timer(ceili(time))
	
	if (
		fighter_left and 
		fighter_right
	):
		if (
			position_difference < 0
			and fighter_left.position.x - fighter_right.position.x >= 0
		):
			fighter_left.avatar.player_side = Defs.Side.RIGHT
			fighter_right.avatar.player_side = Defs.Side.LEFT
		elif (
			position_difference >= 0
			and fighter_left.position.x - fighter_right.position.x < 0
		):
			fighter_left.avatar.player_side = Defs.Side.LEFT
			fighter_right.avatar.player_side = Defs.Side.RIGHT
		position_difference = fighter_left.position.x - fighter_right.position.x


func _on_countdown_finished():
	set_process(true)
	fighter_left.state_machine.set_process(true)
	fighter_right.state_machine.set_process(true)


func _on_fighter_lives_changed(fighter: Fighter, current_lives: int):
	hud.set_hp(
		hud.PLAYER_1 if fighter == fighter_left else hud.PLAYER_2,
		current_lives
	)


func match_finished(loser: Fighter):
	print(loser, " perdeu!!")
	match loser:
		fighter_left:
			fighter_right.state_machine.set_state("Winner")
			fighter_left.state_machine.set_state("Loser")
		fighter_right:
			fighter_left.state_machine.set_state("Winner")
			fighter_right.state_machine.set_state("Loser")
	await get_tree().create_timer(3.0).timeout
	SceneSwitcher.go_to_title()
