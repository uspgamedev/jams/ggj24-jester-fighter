class_name Stage
extends Node2D


@export var fighters: Node2D
@export var fighter_left_placement: Marker2D
@export var fighter_right_placement: Marker2D

	
func place_fighter_left(fighter: Node2D):
	fighter.position = fighter_left_placement.position
	fighters.add_child(fighter)
	fighter.avatar.player_side = Defs.Side.LEFT

func place_fighter_right(fighter: Node2D):
	fighter.position = fighter_right_placement.position
	fighters.add_child(fighter)
	fighter.avatar.player_side = Defs.Side.RIGHT
