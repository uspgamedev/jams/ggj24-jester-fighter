extends CanvasLayer


enum {PLAYER_1, PLAYER_2}

@onready var player_bars = [%PlayerBar1, %PlayerBar2]
@onready var time_label := %TimeLabel
@onready var pre_match_text := %PreMatchText

func setup_player_bars(p1_data: FighterData, p2_data: FighterData) -> void:
	player_bars[PLAYER_1].set_portrait(p1_data.portrait_texture)
	player_bars[PLAYER_2].set_portrait(p2_data.portrait_texture)


func set_timer(time: int) -> void:
	time_label.text = "%d" % time


func set_hp(player: int, hp: int):
	if hp >= 0:
		player_bars[player].life_bar.set_hp(hp)
