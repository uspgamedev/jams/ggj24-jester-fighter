class_name LifeBar
extends MarginContainer


const ANIMATIONS = [
	"third_hit",
	"second_hit",
	"first_hit",
]


func set_hp(hp: int):
	%AnimationPlayer.play(ANIMATIONS[hp])
