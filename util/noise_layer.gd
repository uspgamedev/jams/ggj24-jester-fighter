extends CanvasLayer

@export var textures: Array[Texture2D]
@export_range(0.0, 1.0) var no_noise_chance: float
@export var period_min := 0.1
@export var period_max := 0.2

@onready var texture_rect := $TextureRect as TextureRect
@onready var timer := $Timer as Timer

var current_texture: Texture2D

func _on_tick():
	if randf() <= no_noise_chance:
		texture_rect.texture = null
	else:
		current_texture = textures.filter(
			func(t): return t != current_texture
		).pick_random()
		texture_rect.texture = current_texture
		texture_rect.flip_h = randf() > .5
		texture_rect.flip_v = randf() > .5
	timer.wait_time = randf_range(period_min, period_max)
	timer.start()
