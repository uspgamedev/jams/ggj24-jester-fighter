extends Node


@onready var menu_bgm := $Menu as AudioStreamPlayer
@onready var battle_bgm := $Battle as AudioStreamPlayer

var current: AudioStreamPlayer = null


func play_menu_bgm():
	if current and current != menu_bgm:
		await _fade_out_previous()
	elif current == menu_bgm:
		return
	current = menu_bgm
	current.volume_db = 0.0
	current.play()


func play_battle_bgm():
	if current and current != battle_bgm:
		await _fade_out_previous()
	elif current == battle_bgm:
		return
	current = battle_bgm
	current.volume_db = 0.0
	current.play()


func _fade_out_previous():
	var tween = create_tween()
	tween.tween_property(current, "volume_db", -80.0, 1.0)
	await tween.finished
