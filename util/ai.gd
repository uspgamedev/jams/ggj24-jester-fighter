class_name AI
extends Node


@export var cpu_input: CPUInput


func _ready():
	make_decision()
	

func make_decision():
	cpu_input.set_input("light_attack", 1, 0)
	await get_tree().create_timer(0.8).timeout
	
	make_decision()
