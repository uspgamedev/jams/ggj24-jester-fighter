class_name CPUInput
extends InputManager


func _ready():
	input_buffer = {
		"left": {
			"just_pressed": false,
			"holding": false,
		},
		"right": {
			"just_pressed": false,
			"holding": false,
		},
		"light_attack": {
			"just_pressed": false,
			"holding": false,
		},
		"medium_attack": {
			"just_pressed": false,
			"holding": false,
		},
		"heavy_attack": {
			"just_pressed": false,
			"holding": false,
		},
		"dodge": {
			"just_pressed": false,
			"holding": false,
		},
	}


## An input name, 0 or 1 to represent not pressed or pressed, and 0 or 1 to represent 
## short or long press
func set_input(input: String, press: int = 0, press_type: int = 0):
	# man I'm terribly sorry for this nesting terror, I'll do better next time
	if input_buffer.has(input):
		match press:
			0:
				match press_type:
					0:
						input_buffer[input].just_pressed = false
					1:
						input_buffer[input].holding = false
			1:
				match press_type:
					0:
						input_buffer[input].just_pressed = true
						await get_tree().create_timer(0.1).timeout
						set_input(input, 0, 0)
					1:
						input_buffer[input].holding = true
		#print(input_buffer)
