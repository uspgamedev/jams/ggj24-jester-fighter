extends Node

@export var title_scene: PackedScene
@export var roster_scene: PackedScene
@export var match_scene: PackedScene
@export var credits_scene: PackedScene

var current_scene_root: Node


func go_to_title():
	_change_to_scene(title_scene.instantiate())


func go_to_roster():
	_change_to_scene(roster_scene.instantiate())


func go_to_match(
		stage_scn: PackedScene,
		fighter_left_data: FighterData,
		fighter_right_data: FighterData
	):
	var match_root := match_scene.instantiate() as Match
	match_root.stage_scn = stage_scn
	match_root.fighter_left_data = fighter_left_data
	match_root.fighter_right_data = fighter_right_data
	_change_to_scene(match_root)


func go_to_credits():
	pass


func _change_to_scene(node: Node):
	current_scene_root.queue_free()
	await current_scene_root.tree_exited
	current_scene_root = node
	get_tree().root.add_child(node)
