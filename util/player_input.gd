class_name PlayerInput
extends InputManager


func _ready():
	input_buffer= {
		"up": {
			"just_pressed": false,
			"holding": false,
		},
		"down": {
			"just_pressed": false,
			"holding": false,
		},
		"left": {
			"just_pressed": false,
			"holding": false,
		},
		"right": {
			"just_pressed": false,
			"holding": false,
		},
		"light_attack": {
			"just_pressed": false,
			"holding": false,
		},
		"medium_attack": {
			"just_pressed": false,
			"holding": false,
		},
		"heavy_attack": {
			"just_pressed": false,
			"holding": false,
		},
		"start": {
			"just_pressed": false,
			"holding": false,
		},
		"select": {
			"just_pressed": false,
			"holding": false,
		},
		"dodge": {
			"just_pressed": false,
			"holding": false,
		},
	}


func _process(_delta):
	for input in input_buffer.keys():
		if Input.is_action_just_pressed(input):
			#print("just pressed ", input)
			input_buffer[input].just_pressed = true
		else:
			input_buffer[input].just_pressed = false
		if Input.is_action_pressed(input):
			#print("holding ", input)
			input_buffer[input].holding = true
		else:
			input_buffer[input].holding = false
	#print(input_buffer)
