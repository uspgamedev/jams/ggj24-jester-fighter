class_name Defs
extends Object

enum Side {
	LEFT,
	RIGHT
}

enum Layer {
	PHYSICAL = 1,
	PLAYER_LEFT = 2,
	PLAYER_RIGHT = 4,
}
