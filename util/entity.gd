class_name Entity
extends Node2D


static func get_entity(component: Node) -> Entity:
	var parent = component.get_parent()
	
	while parent != null:
		if parent is Entity:
			return parent

		parent = parent.get_parent()

	return null
