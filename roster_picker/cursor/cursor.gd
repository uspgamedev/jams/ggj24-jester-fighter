class_name Cursor
extends Control


const BOTTOM_LABEL_POS = Vector2i(34, 105)
const TOP_LABEL_POS = Vector2i(34, -40)


@export var text : String
@export var is_bottom := true
@export var color : Color


func _ready():
	modulate = color
	%TopPanel.z_index = 1 if is_bottom else 2
	%BottomPanel.z_index = 2 if is_bottom else 1
	%Label.position = BOTTOM_LABEL_POS if is_bottom else TOP_LABEL_POS
	%Label.text = text
