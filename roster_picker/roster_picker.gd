extends Control


@export var selection_box_scene: PackedScene
@export var fighters: Array[FighterData]
@export var stages: Array[PackedScene]

@onready var _columns: int = %CharacterGrid.columns
@onready var _current_cursor: Cursor = %P1Cursor

var _selected_index := 0
var _roster_size: int
var selected_characters: Array[FighterData] = []


func _ready():
	BGM.play_menu_bgm()
	for fighter in fighters:
		var selection_box := selection_box_scene.instantiate()
		selection_box.set_fighter_data(fighter)
		%CharacterGrid.add_child(selection_box)
		for avatars in [%LeftAvatars, %RightAvatars]:
			var avatar = fighter.avatar_scene.instantiate()
			avatar.hide()
			avatars.add_child(avatar)
	_roster_size = %CharacterGrid.get_child_count()
	%LeftAvatars.get_child(0).show()
	await get_tree().process_frame
	%P1Cursor.position = %CharacterGrid.get_child(0).global_position
	%CPUCursor.position = %CharacterGrid.get_child(0).global_position


func _input(event):
	if event.is_action_pressed("left"):
		_move_selector_horizontal(-1)
	elif event.is_action_pressed("right"):
		_move_selector_horizontal(1)
	elif event.is_action_pressed("up"):
		_move_selector_vertical(-1)
	elif event.is_action_pressed("down"):
		_move_selector_vertical(1)
	elif event.is_action_pressed("light_attack"):
		_select_character()
	elif event.is_action_pressed("medium_attack"):
		_select_character()
	elif event.is_action_pressed("heavy_attack"):
		_select_character()


func _move_selector_horizontal(offset: int):
	@warning_ignore("integer_division")
	var line_start = (_selected_index / _columns) * _columns
	var line_indices = range(
		line_start,
		min(line_start + _columns, _roster_size)
	)
	_selected_index = line_indices[
		wrapi(
			line_indices.find(_selected_index) + offset,
			0,
			line_indices.size()
		)
	]
	_update_selected_character()


func _move_selector_vertical(offset: int):
	var column_indices = range(
		_selected_index % _columns,
		_roster_size,
		_columns
	)
	_selected_index = column_indices[
		wrapi(
			column_indices.find(_selected_index) + offset,
			0,
			column_indices.size()
		)
	]
	_update_selected_character()


func _update_selected_character():
	var avs = %LeftAvatars if _current_cursor == %P1Cursor else %RightAvatars
	for i in avs.get_child_count():
		avs.get_child(i).visible = _selected_index == i


func _select_character():
	selected_characters.append(fighters[_selected_index])
	if _current_cursor == %P1Cursor:
		%CPUCursor.show()
		%RightAvatars.get_child(0).show()
		_current_cursor = %CPUCursor
		_selected_index = 0
	else:
		SceneSwitcher.go_to_match(
			stages[0],
			selected_characters[0],
			selected_characters[1],
		)


func _process(_delta):
	_current_cursor.position = lerp(
		_current_cursor.position,
		%CharacterGrid.get_child(_selected_index).global_position,
		.3
	)
